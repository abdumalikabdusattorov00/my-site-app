package uz.impuls;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MySiteAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(MySiteAppApplication.class, args);
    }

}
